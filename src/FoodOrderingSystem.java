import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingSystem {
    private JButton aButton;
    private JButton bButton;
    private JButton cButton;
    private JButton dButton;
    private JButton eButton;
    private JButton fButton;
    private JPanel root;
    private JTextArea orderlist;
    private JLabel label;
    private JButton checkOutButton;
    private JSpinner Spinner1;
    private JSpinner Spinner2;
    private JSpinner Spinner6;
    private JSpinner Spinner5;
    private JSpinner Spinner4;
    private JSpinner Spinner3;
    int Quantities = 0;
    int Price = 0;
    int TotalPrice = 0;

    SpinnerNumberModel model1 = new SpinnerNumberModel(1, 1, 100, 1);
    SpinnerNumberModel model2 = new SpinnerNumberModel(1, 1, 100, 1);
    SpinnerNumberModel model3 = new SpinnerNumberModel(1, 1, 100, 1);
    SpinnerNumberModel model4 = new SpinnerNumberModel(1, 1, 100, 1);
    SpinnerNumberModel model5 = new SpinnerNumberModel(1, 1, 100, 1);
    SpinnerNumberModel model6 = new SpinnerNumberModel(1, 1, 100, 1);

    void ORDER(String foodname,int Quantity,int PRICE){


        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " +foodname+"×"+Quantity+" ("+PRICE * Quantity+"yen)?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0){
            TotalPrice = TotalPrice + PRICE * Quantity;
            String TotalForStr = String.valueOf(TotalPrice);
            JOptionPane.showMessageDialog(null,"Order for "+foodname+"×"+Quantity
                    +" ("+Quantity*PRICE+"yen) received."
            +"\nThe total price will be "+TotalPrice+"yen");
            String currentText =orderlist.getText();
            orderlist.setText(currentText+foodname+"x"+Quantity+" ("+Price * Quantity+"yen)\n");

            label.setText("Total Price:"+TotalForStr+"yen");
        }
    }
    public FoodOrderingSystem() {

        Spinner1.setModel(model1);
        Spinner2.setModel(model2);
        Spinner3.setModel(model3);
        Spinner4.setModel(model4);
        Spinner5.setModel(model5);
        Spinner6.setModel(model6);

        aButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/gyu.jpeg")
        ));

        bButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/gyucheese.jpeg")
        ));

        cButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/gyucheesekimchi.jpeg")
        ));

        dButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/currychicken.jpeg")
        ));

        eButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/currychickencheese.jpeg")
        ));

        fButton.setIcon(new ImageIcon(
                this.getClass().getResource("images/currybutterchicken.jpeg")
        ));


        aButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Price = 380;
                Quantities = (int) Spinner1.getValue();
                ORDER ("Gyumeshi",Quantities,Price);
            }
        });

        bButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Price = 550;
                Quantities = (int) Spinner2.getValue();
                ORDER ("Cheese Gyumeshi",Quantities,Price);
            }
        });

        cButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Price = 590;
                Quantities = (int) Spinner3.getValue();
                ORDER ("Cheese Kimchi Gyumeshi",Quantities,Price);
            }
        });

        dButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Price = 630;
                Quantities = (int) Spinner4.getValue();
                ORDER ("Chicken Curry",Quantities,Price);
            }
        });

        eButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Price = 780;
                Quantities = (int) Spinner5.getValue();
                ORDER ("Chicken Cheese Curry",Quantities,Price);
            }
        });

        fButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Price = 800;
                Quantities = (int) Spinner6.getValue();
                ORDER ("Butter Chicken Curry",Quantities,Price);
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Finalconfirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout? ",
                        "CheckOut",
                        JOptionPane.YES_NO_OPTION
                );
                if(Finalconfirmation == 0){
                    JOptionPane.showMessageDialog(null,"Thank you! The total price is "
                    +TotalPrice+"yen.");
                    TotalPrice = 0;
                    label.setText("Total Price:0yen");
                    orderlist.setText("");



                }
            }
        });

    }




    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingSystem");
        frame.setContentPane(new FoodOrderingSystem().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }



}
